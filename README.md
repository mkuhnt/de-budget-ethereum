# Decentralized version of Cobudget

## Todos

- Align with Cobudget team
- Plug into UI Prototype
- Synchronize with Accounting project

## How to get started

1. Clone the repository
2. Make sure you have the [truffle framework](http://truffleframework.com) installed
3. Make sure to have access to a ethereum test network or equal (e.g. [ethereum testrpc] (https://github.com/ethereumjs/testrpc))
4. Run the tests ```truffle test```

