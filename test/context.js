var Context = artifacts.require("./Context.sol");

contract('Context', function(accounts) {

  it("should bind the creator of the contract as the owner", function() {
    return Context.deployed().then(function(instance) {
      return instance.owner();
    }).then(function(owner) {
      assert.equal(owner, accounts[0], "The creator of the contract isn't the owner.");
    });
  });

  it("should add the owner as a member", function() {
    var ctx;

    return Context.deployed().then(function(instance) {
      ctx = instance;
      return instance.owner();
    }).then(function(owner) {
      return ctx.memberId(owner);
    }).then(function(memberId) {
      return ctx.members(memberId);
    }).then(function(member) {
      // TODO: Find a way to access the structs properties by name
      assert.equal(member[0], accounts[0], "The owner is not a member.");
    })
  });

  it("should allow to add a member", function() {
    var ctx;

    return Context.deployed().then(function(instance) {
      ctx = instance;
      instance.addMember(accounts[1], 'test');
    }).then(function() {
      return ctx.memberId(accounts[1]);
    }).then(function(memberId) {
      return ctx.members(memberId);
    }).then(function(member) {
      // TODO: Find a way to access the structs properties by name
      assert.equal(member[0], accounts[1], "The member wasn't added.");
    });
  });

  it("should add funds to a member", function() {
    var ctx;

    return Context.deployed().then(function(instance) {
      ctx = instance;
      instance.addMember(accounts[1], 'test');
    }).then(function() {
      ctx.addFund(accounts[1], 200);
    }).then(function() {
      return ctx.funds(accounts[1]);
    }).then(function(funds) {
      assert.equal(funds.toNumber(), 200, "The fund wasn't added.");
    });
  });

  it("should allow to create buckets", function(done) {
    var ctx;
    Context.deployed()
    .then(function(instance) {
      ctx = instance;
      return instance.createBucket("Test", 1000, {from: accounts[0]});
    })
    .then(function(tx) {
      return tx.logs[0].args.bucketId;
    })
    .then(function(bucketId) {
      return ctx.buckets.call(bucketId);
    })
    .then(function(bucket) {
      assert.equal(bucket[0].valueOf(), "Test", "The bucket's name wasn't set.");
      assert.equal(bucket[1].valueOf(), "1000", "The bucket's target fund wasn't set.");
    })
    .then(done);
  });

  it("should set the funding status of a newly created bucket to zero", function(done) {
    var ctx;
    Context.deployed()
    .then(function(instance) {
      ctx = instance;
      return ctx.createBucket("Test", 1000, {from: accounts[0]});
    })
    .then(function(tx) {
      return tx.logs[0].args.bucketId;
    })
    .then(function(bucketId) {
      return ctx.currentFunding.call(bucketId);
    })
    .then(function(currentFunding) {
      assert.equal(currentFunding.valueOf(), 0, "The funding status of a newly created bucket wasn't zero.");
    })
    .then(done);
  });

  it("should allow contributions on buckets", function(done) {
    var ctx;
    var bucketId;
    var availableFunds;

    Context.deployed()
    .then(function(instance) {
      ctx = instance;
      instance.addMember(accounts[1], 'test');
      instance.addFund(accounts[1], 200);
      return instance.createBucket("Test", 1000, {from: accounts[1]});
    })
    .then(function(tx) {
      bucketId = tx.logs[0].args.bucketId;
    })
    .then(function() {
      return ctx.funds.call(accounts[1]);
    })
    .then(function(available) {
      availableFunds = available;
      ctx.contribute(bucketId, 100, {from: accounts[1]});
    })
    .then(function() {
      return ctx.currentFunding.call(bucketId);
    })
    .then(function(currentFunding) {
      assert.equal(currentFunding.valueOf(), 100, "The funding status did not reflect contributions.");
    })
    .then(function() {
      return ctx.funds.call(accounts[1]);
    })
    .then(function(remainingFunds) {
      assert.equal(remainingFunds.valueOf(), availableFunds - 100, "The account's funding was not reduced.");
    })
    .then(done);
  });

  it('should not allow contributions if there is not enough fund', function(done) {
    var ctx;
    var bucketId;
    var availableFunds;

    Context.deployed()
    .then(function(instance) {
      ctx = instance;
      instance.addMember(accounts[1], 'test');
      return instance.createBucket("Test", 1000, {from: accounts[1]});
    })
    .then(function(tx) {
      bucketId = tx.logs[0].args.bucketId;
    })
    .then(function() {
      return ctx.funds.call(accounts[1]);
    })
    .then(function(available) {
      availableFunds = available;
      return ctx.contribute(bucketId, availableFunds + 100, {from: accounts[1]});
    })
    .catch(function(error) {
      if(error.toString().indexOf("invalid JUMP") == -1) {
        assert(false, error.toString());
      }
    })
    .then(done);
  });

});
