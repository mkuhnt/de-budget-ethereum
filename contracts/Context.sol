pragma solidity ^0.4.6;

contract Context {

  address public owner;

  // The list of buckets
  Bucket[] public buckets;

  // the member structure
  mapping (address => uint) public memberId;
  Member[] public members;

  // the funds structure that maps address to their respective funds
  mapping (address => uint) public funds;

  // events
  event BucketAdded(uint bucketId, string name, uint targetFunding);
  event MemberAdded(address who, string name);

  // structures

  struct Member {
    address member;
    string name;
  }

  struct Bucket {
    string name;
    uint fundingTarget;
    uint fundingStatus;
  }

  // modifiers

  modifier onlyOwner {
      if (msg.sender != owner) throw;
      _;
  }

  modifier onlyMembers {
      if (memberId[msg.sender] == 0)
      throw;
      _;
  }

  // constructor

  function Context() {
    owner = tx.origin;

    // Inititalize list (to check against zero later)
    addMember(0, '');
    // and add the owner as a member
    addMember(owner, "Owner");
  }

  // features

  function addMember(address who, string name) public  onlyOwner {
    uint id;
    if (memberId[who] == 0) {
      memberId[who] = members.length;
      id = members.length++;
      members[id] = Member({member: who, name: name});
      MemberAdded(who, name);
    } else {
      id = memberId[who];
      Member m = members[id];
    }
  }

  function addFund(address who, uint howMuch) public onlyOwner {
    if (memberId[who] == 0) {
      throw;
    }
    funds[who] += howMuch;
  }

  function createBucket(string theName, uint target) public onlyMembers {
    uint id = buckets.length++;
    buckets[id] = Bucket({name: theName, fundingTarget: target, fundingStatus: 0});
    BucketAdded(id, theName, target);
  }

  function currentFunding(uint bucketId) public returns (uint funding) {
    funding = buckets[bucketId].fundingStatus;
  }

  function contribute(uint bucketId, uint amount) public onlyMembers {
    if (funds[msg.sender] < amount) {
      throw;
    }
    Bucket b = buckets[bucketId];
    b.fundingStatus += amount;
    funds[msg.sender] -= amount;
  }


}
